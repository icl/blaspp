
     _ ) |      \    __|    |     |
     _ \ |     _ \ \__ \ __ __|__ __|
    ___/____|_/  _\____/   _|    _|

**C++ API for the Basic Linear Algebra Subroutines**

**Innovative Computing Laboratory**

**University of Tennessee**

BLAS++ has moved to <https://github.com/icl-utk-edu/blaspp>
